package it.stefanobizzi.corsi.javase.loopsample;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Insert a number (>= 0): ");
        long number = scanner.nextLong();
        System.out.println(number + "! = " + calculateFactorial(number));
        System.out.println(number + (isPrime(number) ? " is prime" : " is not prime"));
    }

    public static boolean isPrime(final long number) {
        if (number < 2) {
            throw new ArithmeticException("Number must be > or == that 2");
        }

        for (int i = 2; i <= number / i; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static long calculateFactorial(final long number) {
        if (number < 0) {
            throw new ArithmeticException("Number must be >= 0");
        }
        long factorial = 1L;
        for (long i = 1; i <= number; i++) {
            factorial *= i;
        }
        return factorial;
    }
}

























